﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSceneButton : MonoBehaviour
{

    public void ButtonClick(string Map)
    {
        SceneLoader.Instance.LoadScene(Map);
    }
}
