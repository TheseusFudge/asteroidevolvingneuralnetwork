﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsyncBehaviour : MonoBehaviour{

    public bool IsDone { get; private set; }

    public void Init() {
        IsDone = false;
        SceneLoader.Instance.LoadedCallback += SceneLoadDone;
        StartCoroutine(this.Initialize());
    }

    protected virtual IEnumerator Initialize(){
        IsDone = true;
        yield return true;
    }

    protected virtual void SceneLoadDone() {
        
    }

    private void OnDestroy()
    {
        SceneLoader.Instance.LoadedCallback -= SceneLoadDone;
    }

}
