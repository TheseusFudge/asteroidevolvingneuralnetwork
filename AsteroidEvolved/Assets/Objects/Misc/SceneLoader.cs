﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : SceneSingleton<SceneLoader> {

    public static bool SceneLoaded { get; private set; }

    private string currentLoadedScene  = "";
    private TMPro.TextMeshProUGUI infoField;

    public delegate void Loaded();
    public Loaded LoadedCallback;

    private void Awake()
    {
        Canvas canvas = GameObject.FindObjectOfType<Canvas>();
        GameObject tmrpo = new GameObject("LoadingInfo");
        infoField = tmrpo.AddComponent<TMPro.TextMeshProUGUI>();
        infoField.rectTransform.anchoredPosition = new Vector2(1,0);
        infoField.alignment = TMPro.TextAlignmentOptions.BottomLeft;
        infoField.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
        infoField.raycastTarget = false;
        infoField.margin = new Vector4(32.0f, 32.0f, 32.0f, 32.0f);
        infoField.text = "";
    }

    public void LoadScene(string mapName)
    {
        SceneLoaded = false;
        StartCoroutine(Loading(mapName));
    }

    public void CheckStartup() {
        if (SceneManager.sceneCount == 1)
        {
            LoadScene("Menu");
        } else {
            Scene newScene = SceneManager.GetSceneAt(1);
            this.LoadScene(newScene.name);
        }
    }

    IEnumerator Loading(string mapName)
    {
        // Check and unload last scene
        if (SceneManager.sceneCount > 1)
        {
            Scene scene = SceneManager.GetSceneAt(1);
            infoField.text = "Unloading scene " + scene.name;
            yield return new WaitForEndOfFrame();
            AsyncOperation uop = SceneManager.UnloadSceneAsync(scene);
            while(!uop.isDone) {
                yield return new WaitForEndOfFrame();
            }
        }
        // Load the new scene
        infoField.text = "Loading scene " + mapName;
        yield return new WaitForEndOfFrame();
        AsyncOperation op = SceneManager.LoadSceneAsync(mapName, LoadSceneMode.Additive);
        while (!op.isDone)
        {
            yield return new WaitForEndOfFrame();
        }
        currentLoadedScene = mapName;
        Scene newScene = SceneManager.GetSceneAt(1);

        // Load all Initializable AsyncBehaviours in the new scene.
        foreach(GameObject root in newScene.GetRootGameObjects()) {
            foreach(AsyncBehaviour asyncOp in root.GetComponentsInChildren<AsyncBehaviour>()) {
                infoField.text = "Initalizing [" + asyncOp.GetType().ToString()+"]" + asyncOp.name;
                yield return new WaitForEndOfFrame();
                // Call the init and wait for the script to have finished.
                asyncOp.Init();
                while (!asyncOp.IsDone)
                {
                    yield return new WaitForEndOfFrame();
                }
            }
        }
        // Set a static gloabl value to loaded so that script can start their stuff now.
        SceneLoaded = true;
        // Send messages to all gameobjects that want this message.
        if(LoadedCallback != null)
            LoadedCallback.Invoke();
        yield return true;
    }
}
