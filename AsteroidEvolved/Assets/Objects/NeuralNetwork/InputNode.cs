﻿
namespace NeuralNetwork
{
    public class InputNode : NeuralNode
    {
        private float _value;

        public InputNode()
        {
            _value = 0.0f;
        }

        public float GetValue()
        {
            return _value;
        }

        public void SetValue(float value)
        {
            _value = value;
        }
    }
}