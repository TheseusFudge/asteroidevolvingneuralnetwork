﻿
namespace NeuralNetwork
{
    public interface NeuralNode
    {
        float GetValue();
    }
}