﻿using UnityEngine;

namespace NeuralNetwork
{
    public class NeuralConnection
    {
        private NeuralNode _input;
        private float _weight = 1.0f;

        public NeuralConnection(NeuralNode node, float weight) {
            _weight = weight;
            _input = node;
        }

        public void UpdateWeight(float weight) 
        {
            _weight = weight;
        }

        public float GetValue() {
            return _input.GetValue() * _weight;
        }

        public bool Mutate() {
            if (Random.value > 0.9f)
            {
                this._weight += Random.Range(-0.01f, 0.01f);
                return true;
            }
            if (Random.value > 0.9f)
            {
                this._weight = 0.0f;
                return true;
            }
            if (Random.value > 0.9f && Mathf.Abs(this._weight) < 0.1f)
            {
                this._weight += Random.Range(-0.1f, 0.1f);
                return true;
            }
            return false;
        }
    }
}