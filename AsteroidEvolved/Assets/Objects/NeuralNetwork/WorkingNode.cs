﻿using UnityEngine;

namespace NeuralNetwork
{
    public class WorkingNode : NeuralNode
    {
        private NeuralConnection[] _connections;

        public delegate float ActivationFunction(float input);

        protected ActivationFunction Activation = Activations.ActivationLinear;

        public WorkingNode(NeuralConnection[] connections, ActivationFunction activation)
        {
            this._connections = connections;
            this.Activation = activation;
        }

        public virtual float GetValue()
        {
            float sum = 0.0f;
            foreach(NeuralConnection connection in _connections) {
                sum += connection.GetValue();
            }
            return this.Activation(sum);
        }

        public int Mutate() {
            if (Random.value < 0.9)
                return 0;
            int mutations = 0;
            foreach(NeuralConnection c in _connections) {
                if(c.Mutate())
                {
                    mutations++;
                }
            }
            return mutations;
        }

    }
}