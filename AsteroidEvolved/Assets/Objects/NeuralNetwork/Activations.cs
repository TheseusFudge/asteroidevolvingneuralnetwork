﻿using UnityEngine;

namespace NeuralNetwork
{
    public static class Activations
    {
        public static float ActivationLinear(float value)
        {
            return value;
        }

        public static float ActivationBoolean(float value)
        {
            return value > 0.0f ? 1 : 0.0f;
        }

        public static float ActivationSigmoid(float value)
        {
            return 1f / (1f + Mathf.Exp(-value));
        }

        public static float ActivationTangent(float value)
        {
            float epx = Mathf.Exp(value);
            float enx = Mathf.Exp(-value);
            return (epx - enx) / (epx + enx);
        }
    }
}
