﻿using UnityEngine;
using NeuralNetwork;

namespace NeuralNetwork {

    public class ShipNetwork : MonoBehaviour {

        public int HiddenNeuronsPerLayer = 5;

        InputNode[] _inputs;
        WorkingNode[] _outputs;
        WorkingNode[] _layer1, _layer2;

        public LayerMask mask;

        Rigidbody _body;

        Transform _target;

        float StartingDistance = 0.0f;

        bool broken = false;
        int generation = 0;
        public float Fitness = 0.0f;

        [SerializeField]
        string Gene = "Ship";

        private void Init()
        {
            this.generation = 0;
            _body = this.GetComponent<Rigidbody>();
            GameObject[] targets = GameObject.FindGameObjectsWithTag("Target");
            _target = targets[Random.Range(0, targets.Length)].transform;
            StartingDistance = (_target.position - transform.position).magnitude;
            // Set input nodes
            _inputs = new InputNode[10];

            for (int i = 0; i < _inputs.Length; i++)
            {
                _inputs[i] = new InputNode();
            }

            // Setup hidden nodes
            _layer1 = new WorkingNode[HiddenNeuronsPerLayer];
            _layer2 = new WorkingNode[HiddenNeuronsPerLayer];

            // set output nodes
            _outputs = new WorkingNode[4];
            // setup connections

            // setup first hidden layer
            for (int i = 0; i < _layer1.Length; i++)
            {
                // to each input
                NeuralConnection[] connections = new NeuralConnection[_inputs.Length];

                for (int j = 0; j < _inputs.Length; j++)
                {
                    connections[j] = new NeuralConnection(_inputs[j], Random.Range(-1.0f, 1.0f));
                }
                _layer1[i] = new WorkingNode(connections, NeuralNetwork.Activations.ActivationSigmoid);
            }

            // Setup second hidden layer
            for (int i = 0; i < _layer2.Length; i++)
            {
                // to each input
                NeuralConnection[] connections = new NeuralConnection[_layer1.Length];

                for (int j = 0; j < _layer1.Length; j++)
                {
                    connections[j] = new NeuralConnection(_layer1[j], Random.Range(-1.0f, 1.0f));
                }
                _layer2[i] = new WorkingNode(connections, NeuralNetwork.Activations.ActivationSigmoid);
            }

            // Setup second hidden layer
            for (int i = 0; i < _outputs.Length; i++)
            {
                // to each input
                NeuralConnection[] connections = new NeuralConnection[_layer2.Length];

                for (int j = 0; j < _layer2.Length; j++)
                {
                    connections[j] = new NeuralConnection(_layer2[j], Random.Range(-1.0f, 1.0f));
                }
                _outputs[i] = new WorkingNode(connections, NeuralNetwork.Activations.ActivationSigmoid);
            }
        }

        private void Update()
        {
            if (!SceneLoader.SceneLoaded)
                return;
            if (broken)
                return;
            // Get Data
            UpdateInputs();

            // Write Data
            transform.position += new Vector3(_outputs[0].GetValue() - _outputs[1].GetValue(), 0f, _outputs[2].GetValue() - _outputs[3].GetValue());
        }

        void UpdateInputs()
        {
            // Set Target Distance
            Vector3 val = _target.position - transform.position;
            _inputs[0].SetValue(val.magnitude);

            // Set Target Direction
            val.Normalize();
            _inputs[1].SetValue(val.x);
            _inputs[2].SetValue(val.z);

            // Set velocity
            val = _body.velocity;
            _inputs[3].SetValue(val.x);
            _inputs[4].SetValue(val.z);

            // Set Rays
            // Front
            _inputs[5].SetValue(RayResult(transform.forward));

            // Sides
            _inputs[6].SetValue(RayResult(transform.right));
            _inputs[7].SetValue(RayResult(-transform.right));

            // Diagonals
            _inputs[8].SetValue(RayResult(transform.right + transform.forward));
            _inputs[9].SetValue(RayResult(-transform.right + transform.forward));
        }
        float RayResult(Vector3 direction) {
            float distance = 1000.0f;
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(transform.position, direction.normalized, out hit, 10.0f, mask))
            {
                Debug.DrawLine(transform.position, hit.point, Color.green);
                distance = hit.distance;
            } else {
                Debug.DrawLine(transform.position, transform.position + direction.normalized, Color.red);
            }
            return distance;
        }

        public void Mutate(ShipNetwork old) {
            // take the old network but mutate it.
            this._inputs = old._inputs;
            this._outputs = old._outputs;
            this._layer1 = old._layer1;
            this._layer2 = old._layer2;
            this.generation = old.generation;
            int mutations = 0;
            foreach (WorkingNode node in _outputs)
            {
                mutations += node.Mutate();
            }
            foreach (WorkingNode node in _layer1)
            {
                mutations += node.Mutate();
            }
            foreach (WorkingNode node in _layer2)
            {
                mutations += node.Mutate();
            }
            Debug.Log("Mutated "+this.name + " with " + mutations + " mutations.");
        }

        Vector3 initpos;
        private void OnCollisionEnter(Collision collision)
        {
            broken = true;
            Fitness = (StartingDistance - (_target.position - transform.position).magnitude);
            Debug.Log("Collision! " + this.name + " Fitness: "+ Fitness.ToString("0000.00"));
        }
    }
}