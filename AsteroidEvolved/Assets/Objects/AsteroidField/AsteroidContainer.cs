﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AsteroidContainer : AsyncBehaviour {

    public GameObject Prefab;

    public Vector2Int AsteroidSize = new Vector2Int(12, 12);

    public Vector2 AsteroidOffset = new Vector3(1, 1);
    public Vector2 AsteroidRandomPosition = new Vector3(1, 1);
	public Transform[] Asteroids;

    protected override IEnumerator Initialize()
    {
        foreach (Transform t in this.transform) {
            t.position += new Vector3(Random.Range(-AsteroidRandomPosition.x, AsteroidRandomPosition.x), 0, Random.Range(-AsteroidRandomPosition.y, AsteroidRandomPosition.y));
            t.rotation = Random.rotation;

        }
        yield return base.Initialize();
    }
}

[CustomEditor(typeof(AsteroidContainer))]
public class AsteroidContainerEditor : Editor {

    public override void OnInspectorGUI()
    {
        this.DrawDefaultInspector();
        AsteroidContainer ac = (target as AsteroidContainer);
        if(GUILayout.Button("Generate Asteroids")) {
            Transform[] tr = ac.GetComponentsInChildren<Transform>();
            foreach(Transform t in tr) {
                if (t == ac.transform)
                    continue;
                DestroyImmediate(t.gameObject);
            }
            for (int x = 0; x < ac.AsteroidSize.x; x++){
                for (int y = 0; y < ac.AsteroidSize.y; y++)
                {
                    Instantiate(ac.Prefab, new Vector3(x * ac.AsteroidOffset.x,0,y * ac.AsteroidOffset.y), Quaternion.identity).transform.SetParent(ac.transform);
                }
            }
        }
    }
}