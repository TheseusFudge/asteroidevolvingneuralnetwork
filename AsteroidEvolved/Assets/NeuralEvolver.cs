﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralEvolver : MonoBehaviour {

    public int Generation = 0;

    public int ShipsPerGeneration = 20;

    public NeuralNetwork.ShipNetwork Prefab;

    SortedList<float, NeuralNetwork.ShipNetwork> _ships;

    int shipposition = 0;

	void Start () {
        GenerateFirstGeneration();
	}

    void GenerateFirstGeneration() {
        _ships = new SortedList<float, NeuralNetwork.ShipNetwork>();
        for (int i = 0; i < ShipsPerGeneration; i++) {
            NeuralNetwork.ShipNetwork ship = GenerateShip();
            ship.name = "Gene " + i.ToString("00") + " Generation 0";
            _ships.Add(0f, ship);
        }
    }

    NeuralNetwork.ShipNetwork GenerateShip(){

        GameObject[] targets = GameObject.FindGameObjectsWithTag("Start");
        Vector3 position = targets[Random.Range(0, targets.Length)].transform.position;

        NeuralNetwork.ShipNetwork ship = Instantiate(Prefab.gameObject, position, Quaternion.identity).GetComponent<NeuralNetwork.ShipNetwork>();
        ship.Init();

        return ship;
    }

    void MutateGeneration()
    {
        int mutations = 5;


    }

    void Breed()
    {

    }
	
	void Update () {
		
	}
}
