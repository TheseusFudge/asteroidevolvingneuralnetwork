# Neura Network Tests #

These are tests at what neural network can be used in games or games-like applications.

## Multiple ships flying to a point through an asteroid field ##

We have a number of ships that have one goal: they need to fly thorugh an asteroid field without colliding with the asetroids.
The ships have the following inputs: 

float - Distance to target
vec3 (3floats) - Direction from self to target
vec3 - speed of ship
vec3 - acceleration of ship
5 floats - Raycast distance to asteroid in raycast.
	1 Raycast looking forward,
	4 raycasts looking to the sides (up and left, up and right, down and left, down and right.

### Fitness function ###
How far they have come through the field.

### Tests ###

The field will be randomly generated and the asteroids size and position and rotation are different each time.

One test will contain two flights each through one generated asteroid field.
The fitness of each flight is then combined.

### Evolution ###

Mutations will happen:

5% of the Weights will be changed in a Range of (-0.1f, +0.1f).
5% weights will be set to zero.


## How do I get set up? ##

* Download the Unity project and start it with Unity 2018.1.